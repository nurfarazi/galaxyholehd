using UnityEngine;
using UnionAssets.FLE;
using System.Collections;

public class PlayServiceCustomLBExample : MonoBehaviour {

	//example
	private const string LEADERBOARD_ID = "CgkI18jEhsUTEAIQCQ";
	//private const string LEADERBOARD_ID = "REPLACE_WITH_YOUR_ID";


	public GameObject avatar;
	private Texture defaulttexture;

	
	public DefaultPreviewButton connectButton;
	public SA_Label playerLabel;

	public DefaultPreviewButton GlobalButton;
	public DefaultPreviewButton LocalButton;


	public DefaultPreviewButton AllTimeButton;
	public DefaultPreviewButton WeekButton;
	public DefaultPreviewButton TodayButton;




	public DefaultPreviewButton SubmitScoreButton;


	public DefaultPreviewButton[] ConnectionDependedntButtons;
	public CustomLeaderboardFiledsHolder[] lines;


	private GPLeaderBoard loadedLeaderBoard = null;
	private GPCollectionType displayCollection = GPCollectionType.FRIENDS;
	private GPBoardTimeSpan displayTime = GPBoardTimeSpan.ALL_TIME;

	private int score = 100;


	//--------------------------------------
	// INITIALIZATION
	//--------------------------------------

	void Start() //awake()//
	{

		//listen for GooglePlayConnection events
		GooglePlayConnection.instance.addEventListener (GooglePlayConnection.PLAYER_CONNECTED, OnPlayerConnected);
		GooglePlayConnection.instance.addEventListener (GooglePlayConnection.PLAYER_DISCONNECTED, OnPlayerDisconnected);
		GooglePlayConnection.instance.addEventListener(GooglePlayConnection.CONNECTION_RESULT_RECEIVED, OnConnectionResult);

		GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_SUBMITED, OnScoreSubmited);
		GooglePlayManager.instance.addEventListener (GooglePlayManager.SCORE_REQUEST_RECEIVED, OnScoreListLoaded);
		connectSubmitShow();
	}



	public void connectSubmitShow() 
	{
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) 
		{
			
			OnPlayerConnected ();
			//SA_StatusBar.text = "connectOnPlayerConnected ";
		} 
		else
		{

			//SA_StatusBar.text = "connectSubmitShow";
			GooglePlayConnection.instance.connect ();
		
		}


	}


	private void OnConnectionResult(CEvent e) {
		
		GooglePlayConnectionResult result = e.data as GooglePlayConnectionResult;
		SA_StatusBar.text = "Connection Resul:  " + result.code.ToString();


		//GooglePlayManager.instance.submitScoreById(LEADERBOARD_ID, Random.Range(50,200));
	}


	private void OnPlayerConnected() 
	{
		SA_StatusBar.text = "OnPlayerConnected";
			GooglePlayManager.instance.submitScoreById(LEADERBOARD_ID, Random.Range(50,200));

	}

	private void OnScoreSubmited(CEvent e) 
	{
		//SA_StatusBar.text = "OnScoreSubmited";
		GooglePlayResult result = e.data as GooglePlayResult;
		//SA_StatusBar.text = "Score Submit Resul:  " + result.message;

		//Reloading scores list:
		GooglePlayManager.instance.loadTopScores(LEADERBOARD_ID, GPBoardTimeSpan.ALL_TIME, GPCollectionType.GLOBAL, 10);
		GooglePlayManager.instance.loadTopScores(LEADERBOARD_ID, GPBoardTimeSpan.ALL_TIME, GPCollectionType.FRIENDS, 10);
		
			GooglePlayManager.instance.showLeaderBoardById(LEADERBOARD_ID);

	}
	
	//--------------------------------------
	// METHODS
	//--------------------------------------



	public void LoadScore() {
		GooglePlayManager.instance.loadPlayerCenteredScores(LEADERBOARD_ID, displayTime, displayCollection, 10);
	}

	public void OpenUI() {
		GooglePlayManager.instance.showLeaderBoardById(LEADERBOARD_ID);
	}
	


	public void ShowGlobal() {
		displayCollection = GPCollectionType.GLOBAL;
	}

	public void ShowLocal() {
		displayCollection = GPCollectionType.FRIENDS;
	}


	public void ShowAllTime() {
		displayTime = GPBoardTimeSpan.ALL_TIME;
	}
	
	public void ShowWeek() {
		displayTime = GPBoardTimeSpan.WEEK;
	}

	public void ShowDay() {
		displayTime = GPBoardTimeSpan.TODAY;
	}




	//--------------------------------------
	// UNITY
	//--------------------------------------

	void Update() {
		

		
		if(loadedLeaderBoard != null) 
		{


			//Geting current player score
			int displayRank;

			GPScore currentPlayerScore = loadedLeaderBoard.GetCurrentPlayerScore(displayTime, displayCollection);
			if(currentPlayerScore == null) 
			{
				//Player does not have rank at this collection / time
				//so let's show the top score
				//since we used loadPlayerCenteredScores function. we should have top scores loaded if player have to scores at this collection / time
				//https://developer.android.com/reference/com/google/android/gms/games/leaderboard/Leaderboards.html#loadPlayerCenteredScores(com.google.android.gms.common.api.GoogleApiClient, java.lang.String, int, int, int)
				//Asynchronously load the player-centered page of scores for a given leaderboard. If the player does not have a score on this leaderboard, this call will return the top page instead.
				displayRank = 1;
			} 
			else 
			{
				//Let's show 5 results before curent player Rank
				displayRank = Mathf.Clamp(currentPlayerScore.rank - 5, 1, currentPlayerScore.rank);

				//let's check if dosplayRank we what to display before player score is exists
				while(loadedLeaderBoard.GetScore(displayRank, displayTime, displayCollection) == null) {
					displayRank++;
				}
			}




			int i = 1;
			foreach(CustomLeaderboardFiledsHolder line in lines) {

				line.Disable();

				GPScore score = loadedLeaderBoard.GetScore(i, displayTime, displayCollection);
				if(score != null) {
					line.rank.text 			= i.ToString();
					line.score.text 		= score.score.ToString();
					line.playerId.text 		= score.playerId;

					GooglePlayerTemplate player = GooglePlayManager.instance.GetPlayerById(score.playerId);
					if(player != null) {
						line.playerName.text =  player.name;
						if(player.hasIconImage) {
							line.avatar.renderer.material.mainTexture = player.icon;
						} else {
							line.avatar.renderer.material.mainTexture = defaulttexture;
						}

					} else {
						line.playerName.text = "--";
						line.avatar.renderer.material.mainTexture = defaulttexture;
					}
					line.avatar.renderer.enabled = true;

				} else {
					line.Disable();
				}

				i++;
			}
		} else {
			foreach(CustomLeaderboardFiledsHolder line in lines) {
				line.Disable();
			}
		}
		
		
		
	}



	void FixedUpdate1() {


		SubmitScoreButton.text = "Submit Score: " + score;
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) 
		{
			if(GooglePlayManager.instance.player.icon != null) {
				avatar.renderer.material.mainTexture = GooglePlayManager.instance.player.icon;
			}
		}  else 
		{
			avatar.renderer.material.mainTexture = defaulttexture;
		}





		
		
		
		
		
		
		string title = "Connect";
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) {
			title = "Disconnect";
			
			foreach(DefaultPreviewButton btn in ConnectionDependedntButtons) {
				btn.EnabledButton();
			}


			AllTimeButton.Unselect();
			WeekButton.Unselect();
			TodayButton.Unselect();
			
			
			switch(displayTime) {
			case GPBoardTimeSpan.ALL_TIME:
				AllTimeButton.Select();
				break;
			case GPBoardTimeSpan.WEEK:
				WeekButton.Select();
				break;
			case GPBoardTimeSpan.TODAY:
				TodayButton.Select();
				break;
			}
			
			
			
			GlobalButton.Unselect();
			LocalButton.Unselect();
			switch(displayCollection) {
			case GPCollectionType.GLOBAL:
				GlobalButton.Select();
				break;
			case GPCollectionType.FRIENDS:
				LocalButton.Select();
				break;
			}


		} else {
			foreach(DefaultPreviewButton btn in ConnectionDependedntButtons) {
				btn.DisabledButton();
				
			}
			if(GooglePlayConnection.state == GPConnectionState.STATE_DISCONNECTED || GooglePlayConnection.state == GPConnectionState.STATE_UNCONFIGURED) {
				
				title = "Connect";
			} else {
				title = "Connecting..";
			}
		}
		
		connectButton.text = title;
	}


	//--------------------------------------
	// EVENTS
	//--------------------------------------


	private void OnScoreListLoaded() {
		
		//SA_StatusBar.text = "Scores Load Finished";
		
	
		loadedLeaderBoard = GooglePlayManager.instance.GetLeaderBoard(LEADERBOARD_ID);

	}

	private void SubmitScore() {
		GooglePlayManager.instance.submitScoreById(LEADERBOARD_ID, score);
		//SA_StatusBar.text = "Submitiong score: " + (score +1).ToString();
		score ++;
	}


	private void OnPlayerDisconnected() {
		//SA_StatusBar.text = "Player Diconnected";
		playerLabel.text = "Player Diconnected";

	}
	
	
	


}

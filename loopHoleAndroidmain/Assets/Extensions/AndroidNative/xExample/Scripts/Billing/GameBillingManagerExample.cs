﻿////////////////////////////////////////////////////////////////////////////////
//  
// @module Android Native Plugin for Unity3D 
// @author Osipov Stanislav (Stan's Assets) 
// @support stans.assets@gmail.com 
//
////////////////////////////////////////////////////////////////////////////////
 
using UnityEngine;
using UnionAssets.FLE;
using System.Collections;
using UnityEngine.UI;
public class GameBillingManagerExample : MonoBehaviour {

	private static bool _isInited = false;
	
	//--------------------------------------
	//  INITIALIZE
	//--------------------------------------
	public Button slots;

	//replace with your consumable item
	public const string COINS_ITEM = "2500_pack";//"small_coins_bag";

	public const string COINS_ITEM1 = "item1id";//"small_coins_bag";
	
	public const string COINS_ITEM2 = "abc1";//"small_coins_bag";
	
	public const string COINS_ITEM3 = "currency_muffin";//"small_coins_bag";

	//replace with your non-consumable item
	public const string COINS_BOOST = "muffins_1000";


	
	private static bool ListnersAdded = false;
	public static void init() {

		if(ListnersAdded) {
			return;
		}
		
		//Filling product list
		//You can skip this if you alredy did this in Editor settings menu
		AndroidInAppPurchaseManager.instance.addProduct(COINS_ITEM);
		AndroidInAppPurchaseManager.instance.addProduct(COINS_BOOST);
		
		AndroidInAppPurchaseManager.instance.addProduct(COINS_ITEM1);
		AndroidInAppPurchaseManager.instance.addProduct(COINS_ITEM2);
		AndroidInAppPurchaseManager.instance.addProduct(COINS_ITEM3);

		//initilaizing store
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
		
		//listening for purchase and consume events
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_PRODUCT_PURCHASED, OnProductPurchased);
		AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_PRODUCT_CONSUMED,  OnProductConsumed);

		//you may use loadStore function without parametr if you have filled base64EncodedPublicKey in plugin settings
		AndroidInAppPurchaseManager.instance.loadStore();

		ListnersAdded = true;
		
		
	}
	
	//--------------------------------------
	//  PUBLIC METHODS
	//--------------------------------------
	
	
	public static void purchase(string SKU) {
		AndroidInAppPurchaseManager.instance.purchase (SKU);
	}
	
	public static void consume(string SKU) {
		AndroidInAppPurchaseManager.instance.consume (SKU);
	}
	
	//--------------------------------------
	//  GET / SET
	//--------------------------------------
	
	public static bool isInited {
		get {
			return _isInited;
		}
	}
	
	
	//--------------------------------------
	//  EVENTS
	//--------------------------------------
	
	private static void OnProcessingPurchasedProduct(GooglePurchaseTemplate purchase) {
		//some stuff for processing product purchse. Add coins, unlock track, etc

		switch(purchase.SKU) {
		case COINS_ITEM:
			consume(COINS_ITEM);
			break;
			
			
		case COINS_ITEM1:
			consume(COINS_ITEM1);
			break;		
			
			case COINS_ITEM2:
			consume(COINS_ITEM2);
			break;		
			
			case COINS_ITEM3:
			consume(COINS_ITEM3);
			break;		
			
		case COINS_BOOST:
			GameDataExample.EnableCoinsBoost();
			break;
		}
	}
	
	private static void OnProcessingConsumeProduct(GooglePurchaseTemplate purchase) {
		switch(purchase.SKU) {
		case COINS_ITEM:
			GameDataExample.AddCoins(100);
			break;
		}
	}
	
	private static void OnProductPurchased(CEvent e) {
		BillingResult result = e.data as BillingResult;

		//this flag will tell you if purchase is avaliable
		//result.isSuccess


		//infomation about purchase stored here
		//result.purchase

		//here is how for example you can get product SKU
		//result.purchase.SKU

		
		if(result.isSuccess) {
			OnProcessingPurchasedProduct (result.purchase);
		} else {
			AndroidMessage.Create("Product Purchase Failed", result.response.ToString() + " " + result.message);
		}
		
		Debug.Log ("Purchased Responce: " + result.response.ToString() + " " + result.message);
	}
	
	
	private static void OnProductConsumed(CEvent e) {
		BillingResult result = e.data as BillingResult;
		
		if(result.isSuccess) {
			OnProcessingConsumeProduct (result.purchase);
		} else {
			AndroidMessage.Create("Product Cousume Failed", result.response.ToString() + " " + result.message);
		}
		
		Debug.Log ("Cousume Responce: " + result.response.ToString() + " " + result.message);
	}
	
	
	private static void OnBillingConnected(CEvent e) {
		BillingResult result = e.data as BillingResult;
		AndroidInAppPurchaseManager.instance.removeEventListener (AndroidInAppPurchaseManager.ON_BILLING_SETUP_FINISHED, OnBillingConnected);
		
		
		if(result.isSuccess) {
			//Store connection is Successful. Next we loading product and customer purchasing details
			AndroidInAppPurchaseManager.instance.addEventListener (AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
			AndroidInAppPurchaseManager.instance.retrieveProducDetails();

		} 
		
		AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);
		Debug.Log ("Connection Responce: " + result.response.ToString() + " " + result.message);
	}
	
	
	


	private static void OnRetriveProductsFinised(CEvent e) //(BillingResult result)
	{
		Debug.Log("OnRetriveProductsFinised");
		BillingResult result = e.data as BillingResult;
		AndroidInAppPurchaseManager.instance.removeEventListener (AndroidInAppPurchaseManager.ON_RETRIEVE_PRODUC_FINISHED, OnRetriveProductsFinised);
		//AndroidInAppPurchaseManager.ActionRetrieveProducsFinished -= OnRetriveProductsFinised;


		if(result.isSuccess) {

			//Debug.Log("OnRetriveProductsFinised COMPLETE");
			//dispatch(BaseEvent.COMPLETE);
			UpdateStoreData();
			_isInited = true;
			AndroidMessage.Create("Success", "Billing init complete inventory contains: " + AndroidInAppPurchaseManager.instance.inventory.purchases.Count + " products");
			for(int i=0;i<AndroidInAppPurchaseManager.instance.inventory.purchases.Count;i++)
			{
				GooglePurchaseTemplate a=AndroidInAppPurchaseManager.instance.inventory.purchases[i];
				print("product"+i);
				print( "orderId"+ a.orderId  )  ;
				print( "packageName"+ a.packageName  )  ;
				print( "SKU"+ a.SKU  )  ;
				
				print( "developerPayload"+ a.developerPayload  )  ;
				print( "signature"+ a.signature  )  ;
				print( "token"+ a.token  )  ;
				print( "time"+ a.time  )  ;
				print( "originalJson"+ a.originalJson  )  ;
				print( " state"+ a.state  )  ;


			}




		}
		else 
		{
			//Debug.Log("OnRetriveProductsFinised FAILED");
			//dispatch(BaseEvent.FAILED);
			AndroidMessage.Create("Connection Responce", result.response.ToString() + " " + result.message);

		}
	}
	


	private static void UpdateStoreData() 
	{
		//chisking if we already own some consuamble product but forget to consume those
		if(AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(COINS_ITEM)) {
			consume(COINS_ITEM);
		}
		
		if(AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(COINS_ITEM1)) {
			consume(COINS_ITEM1);
		}
		
		if(AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(COINS_ITEM2)) {
			consume(COINS_ITEM2);
		}
		
		if(AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(COINS_ITEM3)) {
			consume(COINS_ITEM3);
		}

		//Check if non-consumable rpduct was purchased, but we do not have local data for it.
		//It can heppens if game was reinstalled or download on oher device
		//This is replacment for restore purchase fnunctionality on IOS


		if(AndroidInAppPurchaseManager.instance.inventory.IsProductPurchased(COINS_BOOST)) {
			GameDataExample.EnableCoinsBoost();
		}


	}








}

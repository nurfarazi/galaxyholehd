﻿using UnityEngine;
using System.Collections;


using UnionAssets.FLE;
using System.Collections;



public class manager : MonoBehaviour 
{

	private const string LEADERBOARD_ID = "CgkI18jEhsUTEAIQCQ";


	private static bool IsFbUserInfoLoaded = false;
	private static bool IsFbFrindsInfoLoaded = false;
	private static bool IsFbAuntifivated = false;

	IEnumerator LoadActivityIndicator()
	{
		
		/*
		 * The indicator is available since Unity 3.5 and can be reached via the Play Settings. 
		 * Select the android platform and there "Resolution and Presentation".
		 * There's a drop down list to choose the loading indicator.
	 * 
	 * static int GetActivityIndicatorStyle();	Gets the current activity indicator style.
		 * 
		 * 
		 * 
		 * 
		 * SetActivityIndicatorStyle		Sets the desired activity indicator style.
											Note that the change will take effect 
											on next call to StartActivityIndicator.

			ActivityIndicator Style (Android Specific).

				Variables

				DontShow			Do not show ActivityIndicator.
				Large				Large (android.R.attr.progressBarStyleLarge).
				InversedLarge		Large Inversed (android.R.attr.progressBarStyleLargeInverse).
				Small				Small (android.R.attr.progressBarStyleSmall).
				InversedSmall		Small Inversed (android.R.attr.progressBarStyleSmallInverse).


				 ios
				 
				DontShow	Do not show ActivityIndicator.
				WhiteLarge	The large white style of indicator (UIActivityIndicatorViewStyleWhiteLarge).
				White		The standard white style of indicator (UIActivityIndicatorViewStyleWhite).
				Gray		The standard gray style of indicator (UIActivityIndicatorViewStyleGray).
		 */
		#if UNITY_IPHONE
		Handheld.SetActivityIndicatorStyle(iOSActivityIndicatorStyle.Gray);
		#elif UNITY_ANDROID
		Handheld.SetActivityIndicatorStyle(AndroidActivityIndicatorStyle.Large);
		#endif
		
		/*

			Starts os activity indicator.

			Please be warned that this informs 
			os ui system 
			to start. 
			For actual animation to take effect you usually need to wait 
			till the end of this frame. 
			So if you want activity indicator to be animated during synced operation,
			please use coroutines.???????????????

		 */
		Handheld.StartActivityIndicator();
		yield return new WaitForSeconds(0);
		Application.LoadLevel("25Nov1");
	}


	void Start () 
	{
		//print ("manager Start");
		GooglePlayConnection.instance.addEventListener (GooglePlayConnection.PLAYER_CONNECTED, OnPlayerConnected);
		GooglePlayConnection.instance.addEventListener(GooglePlayConnection.CONNECTION_RESULT_RECEIVED, OnConnectionResult);	
		GooglePlayManager.instance.addEventListener(GooglePlayManager.SCORE_SUBMITED, OnScoreSubmited);



		SPFacebook.instance.addEventListener(FacebookEvents.FACEBOOK_INITED, 			 OnFbInit);
		SPFacebook.instance.addEventListener(FacebookEvents.AUTHENTICATION_SUCCEEDED,  	 OnFbAuth);
		
		
		SPFacebook.instance.addEventListener(FacebookEvents.USER_DATA_LOADED,  			OnFbUserDataLoaded);

		
		SPFacebook.instance.addEventListener(FacebookEvents.POST_FAILED,  			OnFbPostFailed);
		//SPFacebook.instance.addEventListener(FacebookEvents.POST_SUCCEEDED,   		OnFbPost);

		SPFacebook.instance.Init();
		//connectSubmitShowScore ();

	}

	public void Awake(){
		//connectSubmitShowScore ();
		}


	//fb
	
	public void ConnectInitAUthLoaduserPostFb() 
	{
		StartCoroutine(LoadActivityIndicator());        
		if(!IsFbAuntifivated) 
		{
			SPFacebook.instance.Login("email,publish_actions");
			
		} 
		else 
		{
			// LogOut();
			OnFbAuth();
		}
	}
	
	
	private void OnFbInit() 
	{
		
		if(SPFacebook.instance.IsLoggedIn) 
		{
			//OnAuth();
		} else {
			SA_StatusBar.text = "user Login -> fale";
		}
	}
	
	
	public static void OnFbAuth() 
	{
		
		IsFbAuntifivated = true;
		
		SPFacebook.instance.LoadUserData();
		
		
		SPFacebook.instance.Post 
			(
				link: "http://www.dream71bangladesh.com/",
				linkName: "Galaxy Hole HD",
				linkCaption: "Galaxy Hole HD",
				linkDescription: "i scored "+GameManager.CurrentLevelScore+ "at Level "+GameManager.LevelNum +" and got "+GameManager.LevelStar+" Star",
				picture: "http://www.dream71bangladesh.com/Images/logo.png"
				);
		Handheld.StopActivityIndicator();
		Handheld.Vibrate ();
	}
	
	private void OnFbUserDataLoaded() 
	{
		//SA_StatusBar.text = "User data loaded";
		IsFbUserInfoLoaded = true;
		SPFacebook.instance.userInfo.LoadProfileImage(FacebookProfileImageSize.square);
	}




	//******gplay
	public void connectSubmitShowScore() 
	{
		if(GooglePlayConnection.state == GPConnectionState.STATE_CONNECTED) 
		{
			
			OnPlayerConnected ();

		} 
		else
		{

			GooglePlayConnection.instance.connect ();
			//StartCoroutine(LoadActivityIndicator());        
			
		}
		
		
	}


	private void OnConnectionResult(CEvent e) //this onw will be called before OnPlayerConnected
	{
		
		GooglePlayConnectionResult result = e.data as GooglePlayConnectionResult;
		//GooglePlayManager.instance.submitScoreById(LEADERBOARD_ID,  questionManger.totalPointFromCurrentLevel);
	}
	
	
	private void OnPlayerConnected() 
	{
		Handheld.StopActivityIndicator();
		Handheld.Vibrate ();
	}


	public static void reportScore(int scoreee){

		//GooglePlayManager.instance.submitScoreById(LEADERBOARD_ID,scoreee);
	}

	
	private void OnScoreSubmited(CEvent e) 
	{
		GooglePlayResult result = e.data as GooglePlayResult;

		//GooglePlayManager.instance.showLeaderBoardById(LEADERBOARD_ID);
		Handheld.StopActivityIndicator();
		Handheld.Vibrate ();
		
	}
	

	private static void OnFbPostFailed(){

		Handheld.StopActivityIndicator();
	}


}

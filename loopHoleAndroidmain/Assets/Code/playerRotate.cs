﻿using UnityEngine;
using System.Collections;

public class playerRotate : MonoBehaviour {

	private float speed = 150.0f;

	public float minimum = 1.00F;
	public float maximum = 0.1F;
	
	void Start () {
		

		
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.Rotate(Vector3.forward *speed* Time.deltaTime);

		transform.localScale = new Vector3(Mathf.Lerp(minimum, maximum, Time.time), Mathf.Lerp(minimum, maximum, Time.time), 0);
	}
}

using UnityEngine;
using System.Collections;
using UnityEngine.UI;
public class AllButtonAction : MonoBehaviour {

	private const string TWITTER_ADDRESS = "http://twitter.com/intent/tweet";
	private const string TWEET_LANGUAGE = "en"; 
	private const string RatemeLinkForAndroid = "https://play.google.com/store/apps/details?id=com.Charles.LoopHole";
	private const string RatemeLinkForIOS = "https://play.google.com/store/apps/details?id=com.Charles.LoopHole";


	public Button pausebtnn;

	public Text leveltxt;

	private static bool IsInited = false;

	public Button soundBtn;
	public Sprite muteSprite;
	public Sprite unmuteSprite;

	public static bool Ispause;

	public AudioClip btnsound;
	void Awake(){


#if UNITY_EDITOR

#elif UNITY_ANDROID

#elif UNITY_IPHONE
		GameCenterManager.init();
#else
		//string adUnitId = "unexpected_platform";
#endif


		if(!IsInited) {


		//


			IsInited = true;
		}
	}

	float time=0f;
	 void Update(){
		time=time+Time.deltaTime;

		int temtlevelnumber = GameManager.LevelNum + 1;

		leveltxt.text = ("Level " + temtlevelnumber);
	}

	public void IOSreportScore(){
#if UNITY_EDITOR
		
#elif UNITY_ANDROID
		
#elif UNITY_IPHONE
		GameCenterManager.OnScoreSubmited += OnScoreSubmited;
		GameCenterManager.reportScore(100, leaderBoardId);
#else
		//string adUnitId = "unexpected_platform";
#endif

	}

	#if UNITY_IPHONE


	private void OnAchievementsLoaded(ISN_Result result) {
		
		if(result.IsSucceeded) {
			Debug.Log ("Achievemnts was loaded from IOS Game Center");
			
			foreach(AchievementTemplate tpl in GameCenterManager.achievements) {
				Debug.Log (tpl.id + ":  " + tpl.progress);
			}
		}
		
	}
	
	private void OnAchievementsReset() {
		Debug.Log ("All  Achievemnts was reseted");
	}
	
	private void OnAchievementProgress(CEvent e) {
		Debug.Log ("OnAchievementProgress");
		
		ISN_AcheivmentProgressResult result = e.data as ISN_AcheivmentProgressResult;
		
		if(result.IsSucceeded) {
			AchievementTemplate tpl = result.info;
			Debug.Log (tpl.id + ":  " + tpl.progress.ToString());
		}
		
		
	}

	private void OnLeaderBoarScoreLoaded(CEvent e) {
		ISN_PlayerScoreLoadedResult result = e.data as ISN_PlayerScoreLoadedResult;
		
		if(result.IsSucceeded) {
			GCScore score = result.loadedScore;
			IOSNativePopUpManager.showMessage("Leader Board " + score.leaderboardId, "Score: " + score.score + "\n" + "Rank:" + score.rank);
		}
		
	}

	#endif
	
	public void RateMe(){
		#if UNITY_EDITOR
		Application.OpenURL(RatemeLinkForAndroid);
		#elif UNITY_ANDROID
		Application.OpenURL(RatemeLinkForAndroid);
		#elif UNITY_IPHONE
		Application.OpenURL(RatemeLinkForIOS)
		#else
		
		#endif
	}

	public void FacebookbtnClicked(int number){

	
		manager.OnFbAuth ();

		//fbShare.shareOnFb("Play Loop Hole And beat Your friends score");



	}

	public void ShowLeaderboard(){

		string LEADERBOARD_ID = "CgkI7u6av7ABEAIQBQ";
		#if UNITY_EDITOR


		#elif UNITY_ANDROID
		GooglePlayManager.instance.showLeaderBoardById(LEADERBOARD_ID);
		#elif UNITY_IPHONE
		GameCenterManager.showLeaderBoard(leaderBoardId);
		#else
		
		#endif

		}



	public void twitterbtnClicked(int number){


//		GameCenterManager.OnScoreSubmited += OnScoreSubmited;
		//GameCenterManager.reportScore(100, leaderBoardId);
		ShareToTwitter ("Play Loop Hole And beat Your friends score");
	}

	public void TapToContinue(int number){

		Time.timeScale = 1.0f;
		Application.LoadLevel ("levelSelectionScene");
	}


	public  void restarttheLevel(){

		Application.LoadLevel(Application.loadedLevel);
	}
	public  void GoToLevelPage(){
		Time.timeScale = 1.0f;
		Application.LoadLevel("levelSelectionScene");
	}

	public  void GoToMainMenuPage(){
		
		Application.LoadLevel("MainMenu");
	}


	public void PauseBtn(){

		if (!Ispause) 
		{
			Ispause = true;
			GameOverManager.PauseAnimation();
			Invoke("Pausetimestop",0.5f);
			pausebtnn.interactable=false;
		} 
		else 
		{
			Ispause = false;
			NormalSpeed() ;
			GameOverManager.PauseremoveAnimation();
			pausebtnn.interactable=true;
		}
	}

	public void resumeThegame(){
		Ispause = false;
		NormalSpeed() ;
		GameOverManager.PauseremoveAnimation();
		pausebtnn.interactable=true;
		Time.timeScale = 1.0f;
		GameOverManager.PauseremoveAnimation();
	
	}

	void Pausetimestop()
	{
		Time.timeScale=0.0f;

	}

	public  void NormalSpeed(){
		Time.timeScale = 1.0f;

	}


	public void twitterShareScore(){
		
		ShareToTwitter ("i scored "+GameManager.CurrentLevelScore+ "at Level "+GameManager.LevelNum +" and got "+GameManager.LevelStar+" Star");
	}

	public void NextLEvel(){



		int level= GameManager.LevelNum+1;

		print(level);
		GameManager.LevelNum = level;
		dynamicBtnCreate.gotolevel();
	}
	void ShareToTwitter (string textToDisplay)
	{
		Application.OpenURL(TWITTER_ADDRESS +
		                    "?text=" + WWW.EscapeURL(textToDisplay) +
		                    "&amp;lang=" + WWW.EscapeURL(TWEET_LANGUAGE));
	}

	public void toggelMuteForAllSound()
	{

		if (!AudioListener.pause) 
		{
			AudioListener.pause = true;
			AudioListener.volume = 0; 
			soundBtn.image.sprite=muteSprite;
		} 
		else 
		{
			AudioListener.pause = false;
			AudioListener.volume = 1; 
			soundBtn.image.sprite=unmuteSprite;	
		}
	}

	public void btnsoundd(){

		AudioSource.PlayClipAtPoint(btnsound, Vector3.zero);

	}


}

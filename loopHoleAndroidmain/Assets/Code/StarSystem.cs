﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class StarSystem : MonoBehaviour {


	Sprite[] startSprits ;

	private string[] GameOverText;

	//public Button Startbtn ;

	public static Animator animmaa;

	public Text GMText;
	public Text GMScore;

	public Image StargameOver;

	void Start () {

		startSprits= Resources.LoadAll<Sprite>("gameOverStar"); 
		animmaa = GetComponent <Animator> ();


		GameOverText = new string[]{"no stars (imperfect)","rookie","Amateurish ","All-Star","Stellar!","Astronomical!"};


	}

		public Sprite getStarSpriteof(string fileName)
		{print ("getStarSpriteof ");
			int i;
			for(i=0; i< startSprits.Length; i++) 
			{
				if (string.Compare ( startSprits[i].name, fileName) == 0) 
				{

					return startSprits[i];
				}
				
			}
	
			return null;
	}

	void Update () {

		GMText.text = GameOverText [GameManager.levelTextNum];
		GMScore.text=""+GameManager.CurrentLevelScore;

		if(GameManager.LevelStar==0){

			StargameOver.sprite=startSprits[0];
			GameManager.levelTextNum = 0;
		
		}
		if(GameManager.LevelStar==1){
			StargameOver.sprite=startSprits[1];
			GameManager.levelTextNum = 1;
		}
		if(GameManager.LevelStar==2){
			StargameOver.sprite=startSprits[2];
			GameManager.levelTextNum = 2;
		}
		if(GameManager.LevelStar==3){
			StargameOver.sprite=startSprits[3];
			GameManager.levelTextNum = 3;
		}
		if(GameManager.LevelStar==4){
			StargameOver.sprite=startSprits[4];
			GameManager.levelTextNum = 4;
		}
		if(GameManager.LevelStar==5){
			StargameOver.sprite=startSprits[5];
			GameManager.levelTextNum = 5;
		}
	
	}


	public static void showAndsaveScore(){

		GameManager.CurrentLevelScore = GameManager.GetScoreForLEvels (GameManager.LevelStar, GameManager.LevelNum);
		
		Debug.Log (GameManager.CurrentLevelScore);

		//GMScore.text=""+GameManager.CurrentLevelScore;
		}
}

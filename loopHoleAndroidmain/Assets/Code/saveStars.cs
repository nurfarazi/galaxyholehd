﻿using UnityEngine;
using System.Collections;

/*
 * http://answers.unity3d.com/questions/182755/playerprefs-lmitations.html
 * 
 * http://docs.unity3d.com/ScriptReference/PlayerPrefs.html
On Android they are stored in your phone's internal memory in /data/data/appname/shared_prefs/appname.xml Where appname is the full package identifier e.g. com.mycomany.example No idea about iOS sorry


2
On iOS devices, the PlayerPrefs can be found in your app's plist file. It's located at:

/Apps/ your app's folder /Library/Preferences/ your app's .plist

I used a free MacOS app called iExplorer to find and verify any changes I make to the PlayerPrefs on my iPad app. If anyone has a handier/better way, please share.
"To be able to see the PlayerPrefs which Unity 3D stores on your Android, you'll have to have your device rooted. Rooting is necessary to be able to access the data - Partition. You can find the PlayerPrefs' XML file here: /data/data//shared_prefs/.xml"



DeleteAll	Removes all keys and values from the preferences. Use with caution.
DeleteKey	Removes key and its corresponding value from the preferences.
GetFloat	Returns the value corresponding to key in the preference file if it exists.
GetInt	Returns the value corresponding to key in the preference file if it exists.
GetString	Returns the value corresponding to key in the preference file if it exists.
HasKey	Returns true if key exists in the preferences.
Save	Writes all modified preferences to disk.
SetInt	Sets the value of the preference identified by key.
SetInt	Sets the value of the preference identified by key.
SetString	Sets the value of the preference identified by key.
 */
public class saveStars : MonoBehaviour 
{

	public static int[] levelsStars = new int[50];//dynamicBtnCreate.levelsStars[]

	//int lastUnlockedLevel=0;

	void Start () 
	{
		
		//intilizeLevelStars () ;
		
		//PlayerPrefs.DeleteAll();doRandomUnlock (5) ;
		
		loadLastTimeSaves ();
		
		for(int i=0;i<50;i++)
		{
			
			//print(i+"--> "+levelsStars[i]);
			
			
		}
		
		
	}

	void doRandomUnlock (int numToUnlcok=20) 
	{
		 //numToUnlcok=Random.Range(1,51);
		//print (numToUnlcok);
		int j=0;
		while(j != numToUnlcok)
		{
			int levelIndex=Random.Range(0,50);

			//if(levelsStars[levelIndex] == null)
			{
				int startNum=Random.Range(0,4);
				//print (levelIndex +" "+startNum);
				levelsStars[levelIndex]=startNum;
				//print (levelIndex+ " to "  +startNum);

				PlayerPrefs.SetInt (levelIndex.ToString(), startNum);

			}
			//else
			// continue;

			j++;
		}
		PlayerPrefs.Save() ;
	}


	void intilizeLevelStars () 
	{

		if(PlayerPrefs.HasKey("0"))
			levelsStars[0]=PlayerPrefs.GetInt ("0");
		else
			levelsStars[0]=0;

		for(int i=0;i<50;i++)
		{
			
			levelsStars[i]=-1;
		}
	}
	




	static public  void unlockthislevel (int levelNumber) //not levelIndex
	{
		int levelIndex=levelNumber-1;
		PlayerPrefs.SetInt (levelIndex.ToString(), 0);
		PlayerPrefs.Save() ;

		loadLastTimeSaves ();
	}



	static public  void gotthisstar (int levelNumber,int starAmount) //not `
	{
		int levelIndex=levelNumber-1;

		if(PlayerPrefs.HasKey(levelIndex.ToString()))
		{
			int oldStar=PlayerPrefs.GetInt (levelIndex.ToString());
			if(starAmount > oldStar )
			{
				print (levelIndex+" updated gain "+starAmount);
				PlayerPrefs.SetInt (levelIndex.ToString(), starAmount);
				PlayerPrefs.Save() ;
				loadLastTimeSaves ();

			}

		}
		else
		{

			PlayerPrefs.SetInt (levelIndex.ToString(), starAmount);
			PlayerPrefs.Save() ;
			loadLastTimeSaves ();

		}

	

	}

	
	static void loadLastTimeSaves () 
	{
		
		for(int i=0;i<50;i++)
		{
			string key=i.ToString();
			if(PlayerPrefs.HasKey(key))
			{
				
				levelsStars[i]=PlayerPrefs.GetInt (key);
				//print("got "+i+"  " +levelsStars[i]);
				
			}
			else
				levelsStars[i]=-1;
		}
	}
}

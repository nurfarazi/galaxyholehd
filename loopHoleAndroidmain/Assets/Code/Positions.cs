using UnityEngine;
using System.Collections;

public class Positions : MonoBehaviour {

	public GameObject PlayerObject;
	public GameObject HoleObject;
	public GameObject starObject;

	public GameObject Bthunder;

	public GameObject longblock;
	public GameObject halfblock;
	public GameObject smallblock;

	public GameObject Uturnblock;

	public GameObject wheelblock;
	public GameObject Mangoblock;

	public GameObject Plusblock;

	public GameObject circleblock;
	public GameObject threepyramid;
	public GameObject rotateBlock;
	public GameObject ublock;

	private Vector3 Playerposition;
	private Vector3 Holeposition;


	private Quaternion rotation111OfBlock = Quaternion.identity;
	private Quaternion rotation222OfBlock = Quaternion.identity;
	private Quaternion rotation333OfBlock = Quaternion.identity;
	private Quaternion rotation444OfBlock = Quaternion.identity;
	private Quaternion rotation555OfBlock = Quaternion.identity;
	private Quaternion rotation666OfBlock = Quaternion.identity;


	private Vector3 Block111Pos;
	private Vector3 Block222Pos;
	private Vector3 Block333Pos;
	private Vector3 Block444Pos;
	private Vector3 Block555Pos;
	private Vector3 Block666Pos;



	int levelNumber;





	public void Loadblocks(int Levelnum){

		switch(Levelnum)
		{
		case 1:
			break;
		case 2:

			rotation111OfBlock.eulerAngles = directionForObject(0);

			Block111Pos = LoadPosition(40);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			break;
		case 3:

			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(38);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);



			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(60);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			break;

		case 4:
			
			rotation111OfBlock.eulerAngles = directionForObject(0);
			Block111Pos = LoadPosition(38);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(52);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			
			break;
		case 5:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(13);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(67);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			
			break;
		case 6:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(38);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(40);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation333OfBlock.eulerAngles = directionForObject(2);
			Block333Pos = LoadPosition(42);
			Instantiate(smallblock,Block333Pos,rotation333OfBlock);



			rotation444OfBlock.eulerAngles = directionForObject(0);
			Block444Pos = LoadPosition(76);
			Instantiate(Bthunder,Block444Pos,rotation444OfBlock);


			rotation555OfBlock.eulerAngles = directionForObject(0);
			Block555Pos = LoadPosition(4);
			Instantiate(Bthunder,Block555Pos,rotation555OfBlock);
			break;

		case 7:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(20);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(67);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation333OfBlock.eulerAngles = directionForObject(0);
			Block333Pos = LoadPosition(13);
			Instantiate(Plusblock,Block333Pos,rotation333OfBlock);


			rotation444OfBlock.eulerAngles = directionForObject(2);
			Block444Pos = LoadPosition(24);
			Instantiate(Bthunder,Block444Pos,rotation444OfBlock);

			break;
		case 8:
			rotation111OfBlock.eulerAngles = directionForObject(3);
			Block111Pos = LoadPosition(60);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(17);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			rotation333OfBlock.eulerAngles = directionForObject(1);
			Block333Pos = LoadPosition(56);
			Instantiate(longblock,Block333Pos,rotation333OfBlock);
			
			

			
			break;
		case 9:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(74);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(76);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			rotation333OfBlock.eulerAngles = directionForObject(2);
			Block333Pos = LoadPosition(78);
			Instantiate(Bthunder,Block333Pos,rotation333OfBlock);

			rotation444OfBlock.eulerAngles = directionForObject(2);
			Block444Pos = LoadPosition(43);
			Instantiate(Plusblock,Block444Pos,rotation444OfBlock);
			
			break;
		case 10:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(76);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(13);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);

			rotation333OfBlock.eulerAngles = directionForObject(0);
			Block333Pos = LoadPosition(51);
			Instantiate(Bthunder,Block333Pos,rotation333OfBlock);
			
			rotation444OfBlock.eulerAngles = directionForObject(0);
			Block444Pos = LoadPosition(47);
			Instantiate(Bthunder,Block444Pos,rotation444OfBlock);

//			rotation555OfBlock.eulerAngles = directionForObject(0);
//			Block555Pos = LoadPosition(34);
//			Instantiate(Bthunder,Block555Pos,rotation555OfBlock);
//
//			rotation666OfBlock.eulerAngles = directionForObject(0);
//			Block666Pos = LoadPosition(28);
//			Instantiate(Bthunder,Block666Pos,rotation666OfBlock);
			
			break;

		case 11:
			rotation111OfBlock.eulerAngles = directionForObject(0);
			Block111Pos = LoadPosition(58);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(34);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			rotation333OfBlock.eulerAngles = directionForObject(2);
			Block333Pos = LoadPosition(22);
			Instantiate(Bthunder,Block333Pos,rotation333OfBlock);
			

			
			break;
		case 12:
			rotation111OfBlock.eulerAngles = directionForObject(1);
			Block111Pos = LoadPosition(24);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(56);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);


			rotation333OfBlock.eulerAngles = directionForObject(1);
			Block333Pos = LoadPosition(10);
			Instantiate(Bthunder,Block333Pos,rotation333OfBlock);
			break;



		case 13:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(34);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(38);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);

			break;

		case 14:
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(43);
			Instantiate(Bthunder,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(47);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(11);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);

			break;

		case 15:////////////////////
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(1);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(21);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(34);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(62);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(59);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			

			break;
			
		case 16:////////////////////
//			rotation111OfBlock.eulerAngles = directionForObject(2);
//			Block111Pos = LoadPosition(7);
//			Instantiate(longblock,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(41);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(12);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(46);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			
			rotation111OfBlock.eulerAngles = directionForObject(0);
			Block111Pos = LoadPosition(63);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);

			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(66);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			break;

		case 17:////////////////////
			rotation111OfBlock.eulerAngles = directionForObject(0);
			Block111Pos = LoadPosition(19);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(37);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
//			rotation222OfBlock.eulerAngles = directionForObject(0);
//			Block222Pos = LoadPosition(56);
//			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
//			
			
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(50);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			
			rotation111OfBlock.eulerAngles = directionForObject(0);
			Block111Pos = LoadPosition(15);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			
			rotation111OfBlock.eulerAngles = directionForObject(2);
			Block111Pos = LoadPosition(34);
			Instantiate(smallblock,Block111Pos,rotation111OfBlock);
			break;


		case 18:////////////////////

			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(65);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(18);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(25);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(31);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);

			break;

		case 19:////////////////////
			
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(10);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(55);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(40);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(25);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(60);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			break;
		case 20:////////////////////
			
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(10);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(53);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(55);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			break;
		case 21:////////////////////
			
			
			rotation222OfBlock.eulerAngles = directionForObject(5);
			Block222Pos = LoadPosition(44);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(36);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(15);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(7);
			Block222Pos = LoadPosition(11);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);


			break;
		case 22:////////////////////

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(11);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(55);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(48);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			break;

		case 23:////////////////////
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(31);
			Instantiate(threepyramid,Block222Pos,rotation222OfBlock);
			

			break;
		case 24:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(64);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(66);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(20);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(69);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(23);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);


//			rotation222OfBlock.eulerAngles = directionForObject(2);
//			Block222Pos = LoadPosition(34);
//			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			break;
		case 25:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(40);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			

			break;
		case 26:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(18);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(57);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(42);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(17);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			break;
		case 27:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(67);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(49);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(46);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(52);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			break;

		case 28:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(37);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(6);
			Block222Pos = LoadPosition(58);
			Instantiate(ublock,Block222Pos,rotation222OfBlock);

			break;
		case 29:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(19);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(58);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(6);
			Block222Pos = LoadPosition(25);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			break;
		case 30:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(49);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(13);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(43);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(28);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			break;
		case 31:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(40);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(20);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(56);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			break;
		case 32:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(6);
			Block222Pos = LoadPosition(28);
			Instantiate(ublock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(6);
			Block222Pos = LoadPosition(67);
			Instantiate(ublock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(6);
			Block222Pos = LoadPosition(34);
			Instantiate(ublock,Block222Pos,rotation222OfBlock);
			

			
			break;
		case 33:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(49);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(31);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(36);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(37);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(44);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			

			break;
		
		case 34:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(49);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(36);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			

			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(44);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			

			
			
			break;
		case 35:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(37);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(76);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(15);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(25);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(52);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);


			
			
			
			break;
		case 36:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(1);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(58);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(3);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(78);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);
			
			

			
			
			
			
			break;
		case 37:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(37);
			Instantiate(Mangoblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(34);
			Instantiate(wheelblock,Block222Pos,rotation222OfBlock);

	
			break;

		case 38:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(28);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(46);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(34);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(52);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			break;
		case 39:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(22);
			Instantiate(Bthunder,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(28);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(55);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);




			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(34);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(61);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);


			break;
		case 40:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(11);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(36);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(65);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(67);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

	

	
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(76);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(3);
			Block222Pos = LoadPosition(69);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(44);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(1);
			Block222Pos = LoadPosition(15);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);
			
			break;
		case 41:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(4);
			Block222Pos = LoadPosition(42);
			Instantiate(ublock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(7);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(47);
			Instantiate(halfblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(56);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(19);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			break;

		case 42:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(3);
			Instantiate(smallblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(45);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(39);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(52);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(1);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(25);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			
			break;

		case 43:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(22);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(4);
			Block222Pos = LoadPosition(43);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(4);
			Block222Pos = LoadPosition(37);
			Instantiate(Uturnblock,Block222Pos,rotation222OfBlock);

			break;

		case 44:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(10);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(22);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(16);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(64);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
		
			
			break;

		case 45:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(20);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(20);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(16);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(16);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(73);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			
			
			break;
		case 46:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(73);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(20);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(3);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);

			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(49);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			

			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(78);
			Instantiate(longblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			
			
			break;
		case 47:////////////////////
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(73);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(20);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(3);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(49);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(78);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(43);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(2);
			Block222Pos = LoadPosition(45);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);



			
			break;
		case 48:////////////////////

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(58);
			Instantiate(threepyramid,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(22);
			Instantiate(threepyramid,Block222Pos,rotation222OfBlock);

			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(16);
			Instantiate(rotateBlock,Block222Pos,rotation222OfBlock);

			break;
		case 49:////////////////////
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(37);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(37);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);


			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(40);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(40);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);



			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(44);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(44);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			

			
			break;
		case 50:////////////////////
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(10);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(29);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(31);
			Instantiate(circleblock,Block222Pos,rotation222OfBlock);
			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(51);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			
			

			
			rotation222OfBlock.eulerAngles = directionForObject(0);
			Block222Pos = LoadPosition(40);
			Instantiate(Plusblock,Block222Pos,rotation222OfBlock);
			
			
			
			break;
		default:
			print ("water me. Loadblocks");
			break;
		}

	}

	void Start () 
	{
		if (GameManager.LevelNum==0){
			levelNumber=1;
		} else{
			levelNumber =GameManager.LevelNum+1;
		}
		//levelNumber=10;
		
		heroAndHolePositionSet(levelNumber);
		Loadblocks(levelNumber);
		LoadStars(levelNumber);
		
		Instantiate(PlayerObject,Playerposition,Quaternion.identity);
		Instantiate(HoleObject,Holeposition,Quaternion.identity);
		
		//GameManager.SlowMotion();
	}
	


	public void LoadStars(int Levelnum){
		
		switch(Levelnum)
		{
		case 1:
			Instantiate(starObject,LoadPosition(13),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(31),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(49),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(60),rotation111OfBlock);
			break;
		case 2:

			Instantiate(starObject,LoadPosition(11),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(37),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(56),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(43),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(62),rotation111OfBlock);
			
			break;
		case 3:
			
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(1),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(2),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(49),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(58),rotation111OfBlock);
			
			break;
		case 4:
			
			Instantiate(starObject,LoadPosition(63),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(64),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(65),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(78),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(77),rotation111OfBlock);
			
			
			break;
		case 5:
			
			Instantiate(starObject,LoadPosition(6),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(7),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(12),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(21),rotation111OfBlock);
			break;
		case 6:
			
			Instantiate(starObject,LoadPosition(41),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(39),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(26),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(79),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(78),rotation111OfBlock);
			break;
		case 7:
			
			Instantiate(starObject,LoadPosition(77),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(75),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(1),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(57),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(59),rotation111OfBlock);

			break;
		case 8:
			
			Instantiate(starObject,LoadPosition(77),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(75),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(1),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(53),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
		
			break;
		case 9:
			
			Instantiate(starObject,LoadPosition(77),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(75),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(79),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(6),rotation111OfBlock);
			break;
		case 10:
			
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(73),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(74),rotation111OfBlock);
			break;
		case 11:
			
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(41),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(35),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			break;
		case 12:
			
			Instantiate(starObject,LoadPosition(1),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(35),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(74),rotation111OfBlock);
			break;
		case 13:
			
			Instantiate(starObject,LoadPosition(47),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(33),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(29),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(40),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			break;
		case 14:
			
			Instantiate(starObject,LoadPosition(27),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(2),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(73),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(42),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			break;
		case 15:
			
			Instantiate(starObject,LoadPosition(3),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(77),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(78),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(11),rotation111OfBlock);
			break;
		case 16:
			
			Instantiate(starObject,LoadPosition(3),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(54),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(31),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(33),rotation111OfBlock);
			break;
		case 17:
			
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(54),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(2),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			break;
		case 18:
			
			Instantiate(starObject,LoadPosition(5),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(2),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(3),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(73),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(59),rotation111OfBlock);

			break;
		case 19:
			
			Instantiate(starObject,LoadPosition(25),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(40),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(19),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(55),rotation111OfBlock);
			break;
		case 20:
			
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(56),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			break;
		case 21:
			
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(28),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(4),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(11),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(15),rotation111OfBlock);
			break;
		case 22: 
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(38),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(45),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(57),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(43),rotation111OfBlock);
			break;
		case 23: 
			Instantiate(starObject,LoadPosition(21),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(23),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(25),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(40),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(42),rotation111OfBlock);
			break;
		case 24: 
			Instantiate(starObject,LoadPosition(21),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(24),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(76),rotation111OfBlock);
			break;
		case 25: 
			Instantiate(starObject,LoadPosition(32),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(30),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(48),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(50),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(26),rotation111OfBlock);
			break;
		case 26: 
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(16),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(50),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			break;
		case 27: 
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(76),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(57),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);


			break;
		case 28: 
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(42),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(38),rotation111OfBlock);

			break;
		case 29: 
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(52),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(46),rotation111OfBlock);
			break;
		case 30: 
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(23),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(18),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(4),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(12),rotation111OfBlock);
			break;
		case 31: 
			Instantiate(starObject,LoadPosition(20),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(55),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(48),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(17),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(71),rotation111OfBlock);
			break;
		case 32: 
			Instantiate(starObject,LoadPosition(37),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(43),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(67),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(44),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			break;
		case 33: 
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(16),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(40),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			break;
		case 34: 
			Instantiate(starObject,LoadPosition(37),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(40),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(43),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(55),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
			break;
		case 35: 
			Instantiate(starObject,LoadPosition(29),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(34),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(7),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
		
			break;
		case 36: 
			Instantiate(starObject,LoadPosition(29),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(34),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(9),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(59),rotation111OfBlock);
			break;
		case 37: 
			Instantiate(starObject,LoadPosition(9),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(34),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);

			break;
		case 38: 
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(16),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(76),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(55),rotation111OfBlock);
			break;
		case 39: 
			Instantiate(starObject,LoadPosition(19),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(25),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(5),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(79),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(73),rotation111OfBlock);
			break;
		case 40: 
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(75),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(62),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(16),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);
			break;
		case 41: 
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(15),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(72),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(66),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(46),rotation111OfBlock);
			break;
		case 42: 
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(8),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(2),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(0),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(4),rotation111OfBlock);
			break;
		case 43: 
			Instantiate(starObject,LoadPosition(52),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(46),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(80),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(22),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(24),rotation111OfBlock);
			break;
		case 44: 
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(13),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(15),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(56),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(58),rotation111OfBlock);
			break;
		case 45: 
			Instantiate(starObject,LoadPosition(10),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(13),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(15),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(26),rotation111OfBlock);
			break;
		case 46: 
			Instantiate(starObject,LoadPosition(19),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(22),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(47),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(25),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(79),rotation111OfBlock);
			break;
		case 47: 
			Instantiate(starObject,LoadPosition(21),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(22),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(73),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(37),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(39),rotation111OfBlock);
			break;
		case 48: 
			Instantiate(starObject,LoadPosition(29),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(31),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(33),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(36),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(9),rotation111OfBlock);
	
			break;
		case 49: 
			Instantiate(starObject,LoadPosition(29),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(31),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(33),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(67),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
			break;
		case 50: 
			Instantiate(starObject,LoadPosition(18),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(25),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(48),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(11),rotation111OfBlock);
			Instantiate(starObject,LoadPosition(61),rotation111OfBlock);
		
			break;
		default:
			print ("load star break , oh water me baby star");
			break;
		}
	}

	public Vector3 directionForObject(int num){
		
		Vector3[] positionArray = new []{
			
			new Vector3(0.0f,	0.0f, 0.0f),
			new Vector3(0.0f,	0.0f, 45.0f),
			new Vector3(0.0f,	0.0f, 90.0f),
			new Vector3(0.0f,	0.0f, 135.0f),
			new Vector3(0.0f,	0.0f, 180.0f),
			new Vector3(0.0f,	0.0f, 230.0f),
			new Vector3(0.0f,	0.0f, 270.0f),
			new Vector3(0.0f,	0.0f, 310.0f),
			new Vector3(0.0f,	0.0f, 360.0f),
		};
		return positionArray[num];
	}


	public Vector3 LoadPosition(int num){
		
		Vector3[] positionArray = new []{
			new Vector3(-7.5f,	4.0f, 0.0f),
			new Vector3(-5.5f,	4.0f, 0.0f), 
			new Vector3(-3.5f,	4.0f, 0.0f),
			new Vector3(-1.5f,	4.0f, 0.0f),
			new Vector3(+0.0f,	4.0f, 0.0f),
			new Vector3(+1.5f,	4.0f, 0.0f),
			new Vector3(+3.5f,	4.0f, 0.0f),
			new Vector3(+5.5f,	4.0f, 0.0f),
			new Vector3(+7.5f,	4.0f, 0.0f),

			new Vector3(-7.5f,	3.0f, 0.0f),
			new Vector3(-5.5f,	3.0f, 0.0f), 
			new Vector3(-3.5f,	3.0f, 0.0f),
			new Vector3(-1.5f,	3.0f, 0.0f),
			new Vector3(+0.0f,	3.0f, 0.0f),
			new Vector3(+1.5f,	3.0f, 0.0f),
			new Vector3(+3.5f,	3.0f, 0.0f),
			new Vector3(+5.5f,	3.0f, 0.0f),
			new Vector3(+7.5f,	3.0f, 0.0f),

			new Vector3(-7.5f,	2.0f, 0.0f),
			new Vector3(-5.5f,	2.0f, 0.0f), 
			new Vector3(-3.5f,	2.0f, 0.0f),
			new Vector3(-1.5f,	2.0f, 0.0f),
			new Vector3(+0.0f,	2.0f, 0.0f),
			new Vector3(+1.5f,	2.0f, 0.0f),
			new Vector3(+3.5f,	2.0f, 0.0f),
			new Vector3(+5.5f,	2.0f, 0.0f),
			new Vector3(+7.5f,	2.0f, 0.0f),

			new Vector3(-7.5f,	1.0f, 0.0f),
			new Vector3(-5.5f,	1.0f, 0.0f), 
			new Vector3(-3.5f,	1.0f, 0.0f),
			new Vector3(-1.5f,	1.0f, 0.0f),
			new Vector3(+0.0f,	1.0f, 0.0f),
			new Vector3(+1.5f,	1.0f, 0.0f),
			new Vector3(+3.5f,	1.0f, 0.0f),
			new Vector3(+5.5f,	1.0f, 0.0f),
			new Vector3(+7.5f,	1.0f, 0.0f),

			new Vector3(-7.5f,	0.0f, 0.0f),
			new Vector3(-5.5f,	0.0f, 0.0f), 
			new Vector3(-3.5f,	0.0f, 0.0f),
			new Vector3(-1.5f,	0.0f, 0.0f),
			new Vector3(+0.0f,	0.0f, 0.0f),
			new Vector3(+1.5f,	0.0f, 0.0f),
			new Vector3(+3.5f,	0.0f, 0.0f),
			new Vector3(+5.5f,	0.0f, 0.0f),
			new Vector3(+7.5f,	0.0f, 0.0f),

			new Vector3(-7.5f,	-1.0f, 0.0f),
			new Vector3(-5.5f,	-1.0f, 0.0f), 
			new Vector3(-3.5f,	-1.0f, 0.0f),
			new Vector3(-1.5f,	-1.0f, 0.0f),
			new Vector3(+0.0f,	-1.0f, 0.0f),
			new Vector3(+1.5f,	-1.0f, 0.0f),
			new Vector3(+3.5f,	-1.0f, 0.0f),
			new Vector3(+5.5f,	-1.0f, 0.0f),
			new Vector3(+7.5f,	-1.0f, 0.0f),

			new Vector3(-7.5f,	-2.0f, 0.0f),
			new Vector3(-5.5f,	-2.0f, 0.0f), 
			new Vector3(-3.5f,	-2.0f, 0.0f),
			new Vector3(-1.5f,	-2.0f, 0.0f),
			new Vector3(+0.0f,	-2.0f, 0.0f),
			new Vector3(+1.5f,	-2.0f, 0.0f),
			new Vector3(+3.5f,	-2.0f, 0.0f),
			new Vector3(+5.5f,	-2.0f, 0.0f),
			new Vector3(+7.5f,	-2.0f, 0.0f),

			new Vector3(-7.5f,	-3.0f, 0.0f),
			new Vector3(-5.5f,	-3.0f, 0.0f), 
			new Vector3(-3.5f,	-3.0f, 0.0f),
			new Vector3(-1.5f,	-3.0f, 0.0f),
			new Vector3(+0.0f,	-3.0f, 0.0f),
			new Vector3(+1.5f,	-3.0f, 0.0f),
			new Vector3(+3.5f,	-3.0f, 0.0f),
			new Vector3(+5.5f,	-3.0f, 0.0f),
			new Vector3(+7.5f,	-3.0f, 0.0f),

			new Vector3(-7.5f,	-4.0f, 0.0f),
			new Vector3(-5.5f,	-4.0f, 0.0f), 
			new Vector3(-3.5f,	-4.0f, 0.0f),
			new Vector3(-1.5f,	-4.0f, 0.0f),
			new Vector3(+0.0f,	-4.0f, 0.0f),
			new Vector3(+1.5f,	-4.0f, 0.0f),
			new Vector3(+3.5f,	-4.0f, 0.0f),
			new Vector3(+5.5f,	-4.0f, 0.0f),
			new Vector3(+7.5f,	-4.0f, 0.0f)


		};
		
		
		return positionArray[num];
	}



	public void heroAndHolePositionSet (int levelNumber){

		switch(levelNumber)
		{
		case 1:
			Playerposition = LoadPosition(40);
			Holeposition = LoadPosition(16);
			break;
		case 2:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(13);
			break;
		case 3:
			Playerposition = LoadPosition(73);
			Holeposition = LoadPosition(80);
			break;
		case 4:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(0);
			break;
		case 5:
			Playerposition = LoadPosition(63);
			Holeposition = LoadPosition(80);
			break;
		case 6:
			Playerposition = LoadPosition(73);
			Holeposition = LoadPosition(44);
			break;
		case 7:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(7);
			break;
		case 8:
			Playerposition = LoadPosition(36);
			Holeposition = LoadPosition(80);
			break;
		case 9:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(17);
			break;
		case 10:
			Playerposition = LoadPosition(78);
			Holeposition = LoadPosition(0);
			break;
		case 11:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(6);
			break;
		case 12:
			Playerposition = LoadPosition(40);
			Holeposition = LoadPosition(76);
			break;
		case 13:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(0);
			break;
		case 14:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(77);
			break;
		case 15:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(26);
			break;
		case 16:
			Playerposition = LoadPosition(44);
			Holeposition = LoadPosition(72);
			break;
		case 17:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(42);
			break;
		case 18:
			Playerposition = LoadPosition(71);
			Holeposition = LoadPosition(63);
			break;
		case 19:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(15);
			break;
		case 20:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(4);
			break;
		case 21:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(31);
			break;
		case 22:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(0);
			break;
		case 23:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(8);
			break;
		case 24:
			Playerposition = LoadPosition(74);
			Holeposition = LoadPosition(17);
			break;
		case 25:
			Playerposition = LoadPosition(25);
			Holeposition = LoadPosition(72);
			break;
		case 26:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(76);
			break;
		case 27:
			Playerposition = LoadPosition(63);
			Holeposition = LoadPosition(59);
			break;
		case 28:
			Playerposition = LoadPosition(58);
			Holeposition = LoadPosition(4);
			break;
		case 29:
			Playerposition = LoadPosition(41);
			Holeposition = LoadPosition(70);
			break;
		case 30:
			Playerposition = LoadPosition(75);
			Holeposition = LoadPosition(71);
			break;
		case 31:
			Playerposition = LoadPosition(78);
			Holeposition = LoadPosition(44);
			break;
		case 32:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(40);
			break;
		case 33:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(13);
			break;
		case 34:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(23);
			break;
		case 35:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(8);
			break;
		case 36:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(31);
			break;
		case 37:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(13);
			break;
		case 38:
			Playerposition = LoadPosition(40);
			Holeposition = LoadPosition(13);
			break;
		case 39:
			Playerposition = LoadPosition(58);
			Holeposition = LoadPosition(4);
			break;
		case 40:
			Playerposition = LoadPosition(40);
			Holeposition = LoadPosition(0);
			break;
		case 41:
			Playerposition = LoadPosition(51);
			Holeposition = LoadPosition(0);
			break;
		case 42:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(36);
			break;
		case 43:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(4);
			break;
		case 44:
			Playerposition = LoadPosition(44);
			Holeposition = LoadPosition(72);
			break;
		case 45:
			Playerposition = LoadPosition(78);
			Holeposition = LoadPosition(0);
			break;
		case 46:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(72);
			break;
		case 47:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(10);
			break;
		case 48:
			Playerposition = LoadPosition(80);
			Holeposition = LoadPosition(8);
			break;
		case 49:
			Playerposition = LoadPosition(72);
			Holeposition = LoadPosition(78);
			break;
		case 50:
			Playerposition = LoadPosition(76);
			Holeposition = LoadPosition(8);
			break;
		default:
			print ("water me.??? really ??");
			break;
		}
	}


}

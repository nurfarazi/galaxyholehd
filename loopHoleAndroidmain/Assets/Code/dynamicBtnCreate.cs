﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class dynamicBtnCreate : MonoBehaviour 
{
	static public int presentLevelSelectionStartAt;

	public Button slots;

	int resetValueForX=-500;
	int x ;//= resetValueForX;//-336;//-336.15;
	int y=210;//200.18

	int colDiff=110;
	int rowDiff=-120;


	public Button nextBtn;
	public Button prevBtn;

	Vector3 prevBtnActualPos;
	Vector3 nextBtnActualPos;

	Vector2 positionToMoveTheBtns;


	//int totalRow=2;
	int totalCol=10;

	int totalButtonToCreate;


	public Button[] buttons;
	Text a;

	Sprite[] startSprits ;

	public Sprite unlockSprite;

	Image ownImg;
	Image tempChildImg;

	//public static int[] levelsStars = new int[50];//dynamicBtnCreate.levelsStars[]
	 

	public int getStarSpriteof(string starNumber)
	{
		int i;
		for(i=0; i< startSprits.Length; i++) 
		{
			if (string.Compare ( startSprits[i].name, starNumber) == 0) 
			{

				break;
			}

		}
		return i;
	}




	static public void loadIsReadyScene (int levelIndex) 
	{

		levelIndex = levelIndex-1;

		GameManager.LevelNum=levelIndex;

		Debug.Log ("levelIndex "+GameManager.LevelNum);


	}

	static public void action2PerformOnBtnClicked (int levelIndex) 
	{
		//int newStar=Random.Range(0,4);
		//print ("***gain "+newStar);
		//saveStars.gotthisstar ( levelIndex+1,newStar) ;
		GameManager.LevelNum = levelIndex;
		gotolevel();
		print("new test"+levelIndex);
	}

	static public void gotolevel(){
		 Application.LoadLevel("Game");
	}



	void bindCmd(Button a,int index)
	{
		
		//bind btn Cmd
		int abc =index;
		UnityEngine.Events.UnityAction action = () => {  dynamicBtnCreate.action2PerformOnBtnClicked(abc); };
		a.onClick.AddListener(action);
		
	}

	void Start () 
	{

		startSprits= Resources.LoadAll<Sprite>("spritesForStart"); 

		prevBtnActualPos = prevBtn.transform.position;
		nextBtnActualPos = nextBtn.transform.position;
	
		positionToMoveTheBtns = new Vector2 (-1200, 1200);

	
		Start1 () ;



	}

	void Start1 () 
	{

		if (presentLevelSelectionStartAt == 0) 
		{
			
			prevBtn.transform.position = positionToMoveTheBtns;

		} 
		else  if (presentLevelSelectionStartAt == 40) 
		{

			nextBtn.transform.position = positionToMoveTheBtns;
			
			
		} 
		else 
		{
			
			prevBtn.transform.position = prevBtnActualPos;
			nextBtn.transform.position = nextBtnActualPos;
		}




		x=resetValueForX;

	



		totalButtonToCreate = 50;

		for(int i=0;i<50;i++)
		{	
			//print(i+"--> "+saveStars.levelsStars[i]);
		}

		int countOfChildOnPresentRow=0;
		int levelIndex;//=presentLevelSelectionStartAt;
		for (int i=0; i<  totalButtonToCreate ; i++) 
		 {
			//print (i);
			levelIndex=presentLevelSelectionStartAt+i;
			//create
			Button slot2Add=(Button) Instantiate(slots);
			//print (buttons.Leng  th);
			//buttons[i]=slot2Add;
			//make child
			slot2Add.transform.parent=this.gameObject.transform;

			//position it in parent
			slot2Add.GetComponent<RectTransform>().localPosition=new Vector3(x,y,0);

			//name
			slot2Add.name="slot"+(levelIndex);


			// levelIndexLabel
			a=slot2Add.transform.GetChild(0).GetComponent<Text>();					


			//get the child image
			Image[] allChildImg=slot2Add.GetComponentsInChildren<Image>();

			foreach (Image t in allChildImg) 
			{
				if (string.Compare(t.name,"starDisplayImg") == 0) 
				{
					tempChildImg=t;
				}
				else
				{

				}

				
			}

		

			if((levelIndex==0)||(saveStars.levelsStars[levelIndex] != -1))

			{

				int abc=saveStars.levelsStars[levelIndex];
				print (levelIndex+" open for star "+abc);

				a.text=(levelIndex+1).ToString();
			

				bindCmd(slot2Add,(levelIndex));	

				slot2Add.image.sprite=unlockSprite;

				if(saveStars.levelsStars[levelIndex] == 0)
							tempChildImg.sprite=startSprits[getStarSpriteof("0")] ;
				//StarFromCurrentLevel=1;
				else if(saveStars.levelsStars[levelIndex] == 1)
							tempChildImg.sprite=startSprits[getStarSpriteof("1")] ;//StarFromCurrentLevel=1;
				else if(saveStars.levelsStars[levelIndex] == 2)
							tempChildImg.sprite=startSprits[getStarSpriteof("2")] ;
				else if(saveStars.levelsStars[levelIndex] == 3)
							tempChildImg.sprite=startSprits[getStarSpriteof("3")] ;
				else if(saveStars.levelsStars[levelIndex] == 4)
							tempChildImg.sprite=startSprits[getStarSpriteof("4")] ;
				else if(saveStars.levelsStars[levelIndex] == 5)
							tempChildImg.sprite=startSprits[getStarSpriteof("5")] ;
			}


			countOfChildOnPresentRow++;

			x = x+colDiff;
				

			if(countOfChildOnPresentRow==totalCol)
					{
						x = resetValueForX;
						y=y+rowDiff;//2
						countOfChildOnPresentRow=0;
					}
		}
	}


}

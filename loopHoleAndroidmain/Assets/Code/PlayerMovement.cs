﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerMovement : MonoBehaviour {

    public GameObject player;
	private  float MoveSpeed = 3.0f;
	private float deviceInputX;

	public GameObject bottomExplosion;
	public GameObject rightExplosion;
	public GameObject topExplosion;
	public GameObject LeftExplosion;


	void Start(){

	}

#if UNITY_IPHONE || UNITY_ANDROID || UNITY_EDITOR
	void Update () {

		//deviceInputX= Input.GetAxis("Horizontal") ;
		deviceInputX = progressBar.ControlbarSliderValue;
		//deviceInputX = Input.acceleration.x;

		//Debug.Log(deviceInputX);

		player.transform.Translate(Vector2.up * Time.deltaTime * MoveSpeed, Space.Self);
		
		player.transform.RotateAround(new Vector3(0f, 0f, 3.0f* Time.deltaTime), Time.deltaTime *-deviceInputX);

		if(GameManager.isDead ==0){
			CheckForWallColide();
		}

	}

#endif




	public void CheckForWallColide(){
		if (Mathf.Abs (player.transform.position.x) <-9)
		{
			GameManager.isDead = 1;
			Instantiate(LeftExplosion,player.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
			CollitonCheck.gameOverfailledd();
		}
		if ( Mathf.Abs (player.transform.position.x) >9)
		{
			GameManager.isDead = 1;
			Instantiate(rightExplosion,player.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
			CollitonCheck.gameOverfailledd();
		}
		if (Mathf.Abs (player.transform.position.y) <-5)
		{
			GameManager.isDead = 1;
			Instantiate(bottomExplosion,player.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
			CollitonCheck.gameOverfailledd();
		}
		if ( Mathf.Abs (player.transform.position.y) >5)
		{
			GameManager.isDead = 1;
			Instantiate(topExplosion,player.transform.position, Quaternion.identity);
			Destroy(this.gameObject);
			CollitonCheck.gameOverfailledd();
		}
			

	}





}

﻿using UnityEngine;
using System.Collections;

public class bgMusic : MonoBehaviour {

	public AudioSource SoundSource;
	private static bool created =false;

	void Awake() { 


		if (!created) { 
			// this is the first instance - make it persist 
			DontDestroyOnLoad(this.gameObject); 
			created = true; 
		} 
		else { 
			// this must be a duplicate from a scene reload - DESTROY! 
			Destroy(this.gameObject); 
		}



		SoundSource.playOnAwake = false; 
		SoundSource.rolloffMode = AudioRolloffMode.Logarithmic;
		SoundSource.loop = true; 

	
	}
		
		

	void Start () {
	
		//SoundSource.clip = SoundClip;
		SoundSource.Play();

	}
	
	// Update is called once per frame
	void Update () {


	
	}
}
